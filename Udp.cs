﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class Udp {

	internal enum PacketFlags : byte {
		Simple = 0,
		Complex = 1,
		Ack = 2,
		RemoteCall = 4
	}

	internal class PacketHeader {
		public const int Size = 9;

		public PacketFlags flags;
		public uint messageId;
		public ushort partId;
		public ushort numParts;
	}
	
	internal class Packet {
		public const int Size = 1400;

		public byte[] dgram = new byte[Size];
		public int size;
		internal PacketHeader header = new PacketHeader();
		public EndPoint endPoint;

		public uint writeTick;
		public bool isAcked;
		
		public MemoryStream stream;
		public BinaryWriter writer;
		public BinaryReader reader;
		
		public Packet() {
			endPoint = new IPEndPoint(IPAddress.Any, 0);
			stream = new MemoryStream(dgram);
			writer = new BinaryWriter(stream);
			reader = new BinaryReader(stream);
			size = 0;
		}

		public void CopyDataTo(Packet p) {
			dgram.CopyTo(p.dgram, 0);
			p.size = size;
			p.header.flags = header.flags;
			p.header.messageId = header.messageId;
			p.header.numParts = header.numParts;
			p.header.partId = header.partId;
		}

		public void Reset() {
			reader.BaseStream.Seek(0, SeekOrigin.Begin);
			writer.BaseStream.Seek(0, SeekOrigin.Begin);
			header.flags = PacketFlags.Simple;
			header.messageId = 0;
			header.partId = 0;
			header.numParts = 0;
			isAcked = false;
			writeTick = 0;
			size = 0;
		}

		public void WriteHeader() {
			writer.Seek(0, SeekOrigin.Begin);
			writer.Write((byte)header.flags);
			if ((header.flags & PacketFlags.Complex) != 0) {
				writer.Write(header.messageId);
				writer.Write(header.partId);
				writer.Write(header.numParts);
			}
		}
	}

	public abstract class Message {
		public abstract void ReleasePackets();
		public abstract MessageBuffer GetBuffer();
		public abstract void Send(Remote remote);
	}

	class SimpleMessage : Message {
		public Packet packet;

		public SimpleMessage(Packet packet) {
			this.packet = packet;
		}

		override public void ReleasePackets() {
			Global.packetPool.Add(packet);
		}

		override public MessageBuffer GetBuffer() {
			return new SimpleMessageBuffer(this);
		}

		override public void Send(Remote remote) {
			packet.isAcked = true;
			packet.size = (int)packet.writer.BaseStream.Position;
			remote.SendPacket(packet);
		}
	}

	class AckMessage : SimpleMessage {
		public uint messageId;
		public ushort partId;

		public AckMessage(uint messageId, ushort partId) : base(Global.NewPacket()) {
			this.messageId = messageId;
			this.partId = partId;
		}

		override public void Send(Remote remote) {
			packet.writer.Write(messageId);
			packet.writer.Write(partId);
			packet.header.flags = PacketFlags.Ack;
			base.Send(remote);
		}
	}

	class ComplexMessage : Message {
		public List<Packet> parts = new List<Packet>();
		public uint messageId;
		public int numPartsReceived;

		public uint receivedTick;

		public ComplexMessage() {
		}

		public ComplexMessage(uint messageId) {
			this.messageId = messageId;
			Extend();
		}

		public void SetNumParts(int numParts) {
			for (int i = 0; i < numParts; ++i) {
				parts.Add(null);
			}
		}

		override public void ReleasePackets() {
			Global.packetPool.AddRange(parts);
			parts.Clear();
		}

		override public MessageBuffer GetBuffer() {
			return new ComplexMessageBuffer(this);
		}

		override public void Send(Remote remote) {
			for (int i = 0; i < parts.Count; ++i) {
				parts[i].header.numParts = (ushort)parts.Count;
				parts[i].size = (int)parts[i].writer.BaseStream.Position;
				remote.SendPacket(parts[i]);
			}
		}


		public Packet Extend() {
			Packet newPacket = Global.NewPacket();
			newPacket.header.flags |= PacketFlags.Complex;
			newPacket.header.messageId = messageId;
			newPacket.header.partId = (ushort)parts.Count;
			parts.Add(newPacket);
			return newPacket;
		}
	}

	public abstract class MessageBuffer {
		public abstract Message Message { get; }

		public abstract void Write(byte v);
		public abstract void Write(ushort v);
		public abstract void Write(short v);
		public abstract void Write(uint v);
		public abstract void Write(int v);
		public abstract void Write(float v);
		public abstract void Write(string v);
		public abstract void Write(bool v);
		public abstract void Write(byte[] v, int offset, int count);

		public abstract byte ReadByte();
		public abstract ushort ReadUInt16();
		public abstract short ReadInt16();
		public abstract uint ReadUInt32();
		public abstract int ReadInt32();
		public abstract float ReadSingle();
		public abstract string ReadString();
		public abstract bool ReadBoolean();
		public abstract void ReadBytes(byte[] buffer, int offset, int count);

		public uint ReadLEB128() {
			uint r = 0;
			int s = 0;
			while (true) {
				uint b = ReadByte();
				r += (b & 127) << s;
				if ((b & 128) == 0) {
					break;
				}
				s += 7;
			}
			return r;
		}

		public void WriteLEB128(uint r) {
			do {
				Write((byte)(r & 127));
				r >>= 7;
				if (r != 0) {
					r |= 128;
				}
			} while (r != 0);
		}
	}

	class SimpleMessageBuffer : MessageBuffer {
		SimpleMessage message;
		Packet packet;

		public SimpleMessageBuffer(SimpleMessage message) {
			this.message = message;
			packet = message.packet;
			packet.writer.BaseStream.Seek(1, SeekOrigin.Begin);
			packet.reader.BaseStream.Seek(1, SeekOrigin.Begin);
		}

		override public Message Message { get { return message; } }

		override public void Write(byte v) {
			packet.writer.Write(v);
		}

		override public void Write(ushort v) {
			packet.writer.Write(v);
		}

		override public void Write(short v) {
			packet.writer.Write(v);
		}

		override public void Write(uint v) {
			packet.writer.Write(v);
		}

		override public void Write(int v) {
			packet.writer.Write(v);
		}

		override public void Write(float v) {
			packet.writer.Write(v);
		}

		override public void Write(bool v) {
			packet.writer.Write(v);
		}

		override public void Write(string v) {
			packet.writer.Write(v);
		}

		override public void Write(byte[] buffer, int offset, int count) {
			packet.writer.Write(buffer, offset, count);
		}

		override public byte ReadByte() {
			return packet.reader.ReadByte();
		}

		override public ushort  ReadUInt16() {
			return packet.reader.ReadUInt16();
		}

		override public short ReadInt16() {
			return packet.reader.ReadInt16();
		}

		override public uint ReadUInt32() {
			return packet.reader.ReadUInt32();
		}

		override public int ReadInt32() {
			return packet.reader.ReadInt32();
		}

		override public float ReadSingle() {
			return packet.reader.ReadSingle();
		}

		override public bool ReadBoolean() {
			return packet.reader.ReadBoolean();
		}

		override public string ReadString() {
			return packet.reader.ReadString();
		}

		override public void ReadBytes(byte[] buffer, int offset, int count) {
			packet.reader.Read(buffer, offset, count);
		}
	}

	class ComplexMessageBuffer : MessageBuffer {
		ComplexMessage message;

		int partIndex;
		Packet part;

		public ComplexMessageBuffer(ComplexMessage message) {
			this.message = message;
			partIndex = 0;
			part = message.parts[partIndex];
			part.writer.Seek(PacketHeader.Size, SeekOrigin.Begin);
			part.reader.BaseStream.Seek(PacketHeader.Size, SeekOrigin.Begin);
		}

		override public Message Message { get { return message; } }

		void Extend() {
			part = message.Extend();
			partIndex++;
			part.writer.Seek(PacketHeader.Size, SeekOrigin.Begin);
		}

		void Extend(int size) {
			if (part.writer.BaseStream.Position + size - Packet.Size > 0) {
				Extend();
			}
		}

		void MoveToNextPart() {
			partIndex++;
			part = message.parts[partIndex];
			part.reader.BaseStream.Seek(PacketHeader.Size, SeekOrigin.Begin);
		}

		void PrepareRead(int size) {
			if (part.reader.BaseStream.Position + size - Packet.Size > 0) {
				MoveToNextPart();
			}
		}

		override public void Write(byte v) {
			Extend(1);
			part.writer.Write(v);
		}

		override public void Write(ushort v) {
			Extend(2);
			part.writer.Write(v);
		}

		override public void Write(short v) {
			Extend(2);
			part.writer.Write(v);
		}

		override public void Write(uint v) {
			Extend(4);
			part.writer.Write(v);
		}

		override public void Write(int v) {
			Extend(4);
			part.writer.Write(v);
		}

		override public void Write(float v) {
			Extend(4);
			part.writer.Write(v);
		}

		override public void Write(bool v) {
			Extend(1);
			part.writer.Write(v);
		}

		override public void Write(string v) {
			// NOTE:
			// Encoded string cannot be larger that Global.buffer size
			//
			int count = Encoding.UTF8.GetByteCount(v);

			WriteLEB128((uint)count);
			Encoding.UTF8.GetBytes(v, 0, v.Length, Global.buffer, 0);
			Write(Global.buffer, 0, count);
		}

		override public void Write(byte[] buffer, int offset, int count) {
			while (count != 0) {
				int s = Packet.Size - (int)part.writer.BaseStream.Position;
				if (s > count) {
					s = count;
				}
				part.writer.Write(buffer, offset, s);

				count -= s;
				offset += s;

				if (count != 0) {
					Extend();
				}
			}
		}

		override public byte ReadByte() {
			PrepareRead(1);
			return part.reader.ReadByte();
		}

		override public ushort ReadUInt16() {
			PrepareRead(2);
			return part.reader.ReadUInt16();
		}

		override public short ReadInt16() {
			PrepareRead(2);
			return part.reader.ReadInt16();
		}

		override public uint ReadUInt32() {
			PrepareRead(4);
			return part.reader.ReadUInt32();
		}

		override public int ReadInt32() {
			PrepareRead(4);
			return part.reader.ReadInt32();
		}

		override public float ReadSingle() {
			PrepareRead(4);
			return part.reader.ReadSingle();
		}

		override public bool ReadBoolean() {
			PrepareRead(1);
			return part.reader.ReadBoolean();
		}

		override public string ReadString() {
			int count = (int)ReadLEB128();
			ReadBytes(Global.buffer, 0, count);
			return Encoding.UTF8.GetString(Global.buffer, 0, count);
		}

		override public void ReadBytes(byte[] buffer, int offset, int count) {
			while (count != 0) {
				int s = Packet.Size - (int)part.writer.BaseStream.Position;
				if (s > count) {
					s = count;
				}
				part.reader.Read(buffer, offset, s);

				count -= s;
				offset += s;

				if (count != 0) {
					MoveToNextPart();
				}
			}
		}
	}

	public delegate void RemoteCallHandler(MessageBuffer buffer);

	internal class RemoteCallHandlers {
		internal Dictionary<ushort, RemoteCallHandler> listeners = new Dictionary<ushort, RemoteCallHandler>();


		internal bool HasHandler(ushort endpoint) {
			return listeners.ContainsKey(endpoint);
		}

		internal void Invoke(ushort endpoint, MessageBuffer buffer) {
			listeners[endpoint](buffer);
		}

		internal void SetHandler(ushort endpoint, RemoteCallHandler handler) {
			listeners[endpoint] = handler;
		}
	}

	public class Remote {
		EndPoint endPoint;
		IPEndPoint ipEndPoint;
		Udp udp;
		int id;

		List<ComplexMessage> unreceivedComplex = new List<ComplexMessage>();
		List<ComplexMessage> receivedComplex = new List<ComplexMessage>();
		Queue<Message> receivedQueue = new Queue<Message>();

		List<Packet> notAckedPackets = new List<Packet>();

		public Remote(Udp udp, EndPoint endPoint) {
			this.udp = udp;
			this.endPoint = endPoint;
			ipEndPoint = (IPEndPoint)endPoint;
			id = Global.remoteCounter++;
		}

		public IPEndPoint IPEndPoint { get { return ipEndPoint; } }
		public int Id { get { return id; } }

		public override int GetHashCode() {
			return id;
		}

		public override bool Equals(object obj) {
			return Equals(obj as Remote);
		}

		public bool Equals(Remote obj) {
			return obj != null && obj.id == this.id;
		}

		bool RecentlyReceived(Packet packet) {
			bool received = false;
			for (int i = 0; i < receivedComplex.Count;) {
				if (receivedComplex[i].messageId == packet.header.messageId) {
					SendAck(packet.header.messageId, packet.header.partId);
					received = true;
					++i;
				} else if (udp.tickCounter - receivedComplex[i].receivedTick > udp.recentTimeout) {
					receivedComplex.RemoveAt(i);
				} else {
					++i;
				}
			}
			return received;
		}

		internal void ReceivePacket(Packet packet) {
			if ((packet.header.flags & PacketFlags.Ack) != 0) {
				ReceiveAck(packet);
			} else if ((packet.header.flags & PacketFlags.Complex) != 0) {
				ReceiveComplexMessage(packet);
			} else {
				ReceiveSimpleMessage(packet);
			}
		}

		void ReceiveAck(Packet packet) {
			uint messageId = packet.reader.ReadUInt32();
			ushort partId = packet.reader.ReadUInt16();
			for (int i = 0; i < notAckedPackets.Count; ++i) {
				if (notAckedPackets[i].header.messageId == messageId
				&& notAckedPackets[i].header.partId == partId) {
					Global.packetPool.Add(notAckedPackets[i]);
					notAckedPackets.RemoveAt(i);
					break;
				}
			}
			Global.packetPool.Add(packet);
		}

		void ReceiveComplexMessage(Packet packet) {
			ComplexMessage message = null;

			for (int i = 0; i < unreceivedComplex.Count;) {
				if (unreceivedComplex[i].messageId == packet.header.messageId) {
					message = unreceivedComplex[i];
					break;
				} else {
					if (udp.tickCounter > unreceivedComplex[i].receivedTick + udp.duplicateTimeout) {
						unreceivedComplex.RemoveAt(i);
					} else {
						i++;
					}
				}
			}
			if (message == null) {
				if (RecentlyReceived(packet)) {
					Global.packetPool.Add(packet);
					return;
				}

				message = new ComplexMessage();
				message.messageId = packet.header.messageId;
				message.SetNumParts(packet.header.numParts);
				message.receivedTick = udp.tickCounter;
				unreceivedComplex.Add(message);
			}

			if (message.parts[packet.header.partId] == null) {
				message.parts[packet.header.partId] = packet;
				message.numPartsReceived++;

				SendAck(packet.header.messageId, packet.header.partId);

				if (message.numPartsReceived == message.parts.Count) {
					unreceivedComplex.Remove(message);
					receivedComplex.Add(message);

					receivedQueue.Enqueue(message);
				}
			} else {
				Global.packetPool.Add(packet);
			}
		}

		void ReceiveSimpleMessage(Packet packet) {
			SimpleMessage message = new SimpleMessage(packet);
			receivedQueue.Enqueue(message);
		}

		void SendAck(uint messageId, ushort partId) {
			SimpleMessage message = new AckMessage(messageId, partId);
			EndWrite(message.GetBuffer());
		}

		public bool CanRead {
			get {
				return receivedQueue.Count > 0;
			}
		}

		public MessageBuffer BeginRead() {
			return receivedQueue.Dequeue().GetBuffer();
		}

		public void EndRead(MessageBuffer buffer) {
			buffer.Message.ReleasePackets();
		}

		public MessageBuffer BeginWriteSimple() {
			SimpleMessage message = new SimpleMessage(Global.NewPacket());
			return message.GetBuffer();
		}
		
		public MessageBuffer BeginWriteComplex() {
			ComplexMessage message = new ComplexMessage(udp.messageCounter++);
			return message.GetBuffer();
		}

		public void EndWrite(MessageBuffer buffer) {
			buffer.Message.Send(this);
		}

		internal void SendPacket(Packet packet) {
			packet.WriteHeader();

			if (!packet.isAcked) {
				notAckedPackets.Add(packet);
			} else {
				WritePacket(packet);
				Global.packetPool.Add(packet);
			}
		}

		internal virtual void WritePacket(Packet packet) {
			udp.socket.SendTo(packet.dgram, packet.size, SocketFlags.None, endPoint);
		}

		internal void Update() {
			for (int i = 0; i < notAckedPackets.Count; ++i) {
				if (udp.tickCounter - notAckedPackets[i].writeTick > udp.packetTimeout) {
					WritePacket(notAckedPackets[i]);
					notAckedPackets[i].writeTick = udp.tickCounter;
				}
			}
		}
	}

	static class Global {
		public static List<Packet> packetPool = new List<Packet>();
		public static int remoteCounter = 0;
		public static byte[] buffer = new byte[65536];

		public static Packet NewPacket() {
			if (packetPool.Count > 0) {
				Packet packet = packetPool[packetPool.Count - 1];
				packetPool.RemoveAt(packetPool.Count - 1);
				packet.Reset();
				return packet;
			} else {
				Packet packet = new Packet();
				return packet;
			}
		} 
	}

	public uint packetTimeout = 10;
	public uint duplicateTimeout = 720;
	public uint recentTimeout = 720;
	internal uint tickCounter;
	internal uint messageCounter = 0;

	internal Socket socket;

	public List<Remote> remotes = new List<Remote>();
	Remote broadcast;
	List<IPAddress> loopbackIPs= new List<IPAddress>();
	
	public Udp() {
		socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

#if UNITY_ANDROID
		//NOTE: Android version of Mono does not support System.Net.NetworkInformation.LinuxNetworkInterface:getifaddrs
		foreach (var a in Dns.GetHostAddresses(Dns.GetHostName())) {
			loopbackIPs.Add(a);
		}
		loopbackIPs.Add(IPAddress.Loopback);
#else
		foreach (var adapter in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces()) {
			foreach (var unicastAddress in adapter.GetIPProperties().UnicastAddresses) {
				if (unicastAddress.Address.AddressFamily == AddressFamily.InterNetwork 
					&& unicastAddress.IsDnsEligible) {
					loopbackIPs.Add(unicastAddress.Address);
				}
			}
		}
#endif
	}

	bool IsLoopbackIP(IPAddress address) {
		for (int i = 0; i < loopbackIPs.Count; ++i) {
			if (loopbackIPs[i].Equals(address)) {
				return true;
			}
		}
		return false;
	}

	public Remote GetBroadcast(int port) {
		if (broadcast == null) {
			IPEndPoint broadcastEndpoint = new IPEndPoint(IPAddress.Broadcast, port);
			socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, true);
			broadcast = new Remote(this, broadcastEndpoint);
		}
		return broadcast;
	}

	public Remote GetRemote(EndPoint endPoint) {
		IPEndPoint ipEndPoint = (IPEndPoint)endPoint;
		Remote remote = null;
		for (int i = 0; i < remotes.Count; ++i) {
			if (remotes[i].IPEndPoint.Port == ipEndPoint.Port && remotes[i].IPEndPoint.Address.Equals(ipEndPoint.Address)) {
				remote = remotes[i];
				break;
			}
		}

		bool isLoopback = false;
		if (remote == null) {
			isLoopback = IsLoopbackIP(ipEndPoint.Address);
			if (isLoopback) {
				for (int i = 0; i < remotes.Count; ++i) {
					if (remotes[i].IPEndPoint.Port == ipEndPoint.Port && IPAddress.IsLoopback(remotes[i].IPEndPoint.Address)) {
						remote = remotes[i];
						break;
					}
				}
			}
		}

		if (remote == null) {
			//NOTE: Don't know if it's okay to map all local interfaces addresses to loopback for lookup purpose. 
			if (isLoopback) {
				endPoint = new IPEndPoint(IPAddress.Loopback, ipEndPoint.Port);
			}

			remote = new Remote(this, endPoint);
			remotes.Add(remote);
		}
		return remote;
	}

	public Remote GetRemote(string host, int port) {
		EndPoint endPoint = new IPEndPoint(IPAddress.Parse(host), port);
		return GetRemote(endPoint);
	}

	public void Listen(int port) {
		socket.Bind((EndPoint)(new IPEndPoint(IPAddress.Any, port)));
	}

	public void AllowAddressReuse() {
		socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
	}

	public void Close() {
		socket.Close();
	}

	public void Update() {
		tickCounter++;

		while (socket.Available > 0) {
			Packet packet = Global.NewPacket();

			packet.size = socket.ReceiveFrom(packet.dgram, SocketFlags.None, ref packet.endPoint);

			packet.header.flags = (PacketFlags)packet.reader.ReadByte();
			if ((packet.header.flags & PacketFlags.Complex) != 0) {
				packet.header.messageId = packet.reader.ReadUInt32();
				packet.header.partId = packet.reader.ReadUInt16();
				packet.header.numParts = packet.reader.ReadUInt16();
			}

			if ((packet.header.flags & PacketFlags.RemoteCall) != 0) {
				//TODO: RemoteCall
			} else {
				Remote remote = GetRemote(packet.endPoint);
				remote.ReceivePacket(packet);
			}
		}

		for (int i = 0; i < remotes.Count; ++i) {
			remotes[i].Update();
		}
	}
}

